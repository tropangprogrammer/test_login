﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestLogin.Models;

namespace TestLogin.Controllers
{
    public class DataInputtedController : Controller
    {
        private readonly TestLoginContext _context;

        public DataInputtedController(TestLoginContext context)
        {
            _context = context;
        }

        // GET: DataInputted
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> ViewData2()
        {
            return View(await _context.DataInputted.ToListAsync());
        }

        // GET: DataInputted/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DataInputted/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password")] DataInputted dataInputted)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dataInputted);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: DataInputted/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataInputted = await _context.DataInputted.SingleOrDefaultAsync(m => m.ID == id);
            if (dataInputted == null)
            {
                return NotFound();
            }
            return View(dataInputted);
        }

        // POST: DataInputted/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Username,Password")] DataInputted dataInputted)
        {
            if (id != dataInputted.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dataInputted);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DataInputtedExists(dataInputted.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dataInputted);
        }

        // GET: DataInputted/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dataInputted = await _context.DataInputted
                .SingleOrDefaultAsync(m => m.ID == id);
            if (dataInputted == null)
            {
                return NotFound();
            }

            return View(dataInputted);
        }

        // POST: DataInputted/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dataInputted = await _context.DataInputted.SingleOrDefaultAsync(m => m.ID == id);
            _context.DataInputted.Remove(dataInputted);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DataInputtedExists(int id)
        {
            return _context.DataInputted.Any(e => e.ID == id);
        }
    }
}
