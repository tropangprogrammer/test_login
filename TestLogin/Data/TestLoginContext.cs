﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TestLogin.Models
{
    public class TestLoginContext : DbContext
    {
        public TestLoginContext (DbContextOptions<TestLoginContext> options)
            : base(options)
        {
        }

        public DbSet<TestLogin.Models.DataInputted> DataInputted { get; set; }
    }
}
